/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var path = require('path');
var mongo = require('mongodb');
var mongoUtil = require(path.join(__dirname,'..','services','mongoUtil'));

// Connect to Mongo
mongoUtil.connect('svcc');

var Users = {
  getAll: function (callback) {
    var collection = mongo.DB.collection('users');

    collection.find().toArray(function (err, docs) {
      console.log('query result getAll: ' + JSON.stringify(docs,null,2));
      callback(err, docs);
    });
  },

  findByEmail: function (email, callback) {
    var collection = mongo.DB.collection('users');

    if (!typeof email === 'string') {
      callback(true);
    }
    var query = {email: email};
    collection.find(query).limit(1).next(function (err, doc) {
      console.log('query result findByEmail<'  + email + '>: ' + JSON.stringify(doc,null,2));
      callback(err, doc);
    });
  },

  createUser: function(user,callback) {
    var collection = mongo.DB.collection('users');

    collection.insertOne(user, function(err,result){
      console.log('query result createUser\n' + JSON.stringify(user,null,2) + ': ' + result);
      callback(err,result);
    });
  }
};

module.exports = Users;
