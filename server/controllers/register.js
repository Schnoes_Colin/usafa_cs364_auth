var express = require('express');
var authUtil = require('../services/authUtil');
var Users = require('../models/users');
var bcrypt = require('bcryptjs');

var router = express.Router();

router
  .get('/', function(req, res) {
    res.render('register', { csrfToken: req.csrfToken() });
  })

  .post('/', function(req, res) {

    var salt = bcrypt.genSaltSync(12);
    var hash = bcrypt.hashSync(req.body.password, salt);

    var user = {
      firstName:  req.body.firstName,
      lastName:   req.body.lastName,
      email:      req.body.email,
      password:   hash
    }

    Users.createUser(user,function(err,result) {
      if (err) {
        var error = 'Something bad happened! Please try again';

        if (err.code === 11000) {
          error = 'That email is already taken, please try another';
        }

        res.render('register', {error:error, csrfToken: req.csrfToken() });
      } else {
        authUtil.createUserSession(req, res, user);
        res.redirect('/dashboard');
      }
    })

  });

module.exports = router;
