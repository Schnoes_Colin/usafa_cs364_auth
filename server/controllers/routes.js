var express = require('express')
var router = express.Router();
var path = require('path');
var authUtil = require('../services/authUtil');

var login = require('./login');
var Users = require('../models/users');

var app = express();

var login = require(path.join(__dirname,'login'));
var register = require(path.join(__dirname,'register'));

router
  .use('/register', register)
  .use('/login',login)

  .get('/', function(req, res) {
    res.render('index');
  })

  .get('/dashboard', authUtil.requireLogin('/login'), function(req,res) {
    res.render('dashboard');
  })

  .get('/logout', function(req, res) {
    if (req.session) {
      req.session.reset();
    }
    res.redirect('/');
  })

  .get('/adminpanel', authUtil.requirePermissions(['admin'],'/dashboard'), function(req,res) {
    Users.getAll(function(err,users) {
      res.render('adminpanel');
    });
  })

  .all('*',function(req,res) {
    res.redirect('dashboard');
  });

module.exports = router;
