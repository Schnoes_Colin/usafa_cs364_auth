# svcc-auth

This project is a fork of [rdegges svcc-auth][] and contains code which shows how to implement your own user authentication in a Node.js web app with MongoDB.

The [talk slides][] are in the root of the repository.

rdegges provided a presentation as he developed this security application which is available on [YouTube][].


## Installation

**NOTE**: You must have MongoDB installed and working locally in order to run
this project.

Installing this project is simple, run the following commands (mongod server must be running):

```console
$ git clone https://wbwatkinson@bitbucket.org/wbwatkinson/usafa_cs364_auth.git
$ cd usafa_cs364_auth
$ npm install
$ mongoimport --db svcc --collection users --type json --jsonArray --file server/users-seed.json --jsonArray --drop
```

Within mongo execute the following to force email to be unique within the users collection:
```javascript
use svcc
db.users.createIndex( {"email": 1}, { unique: true } )
exit
```

```console
$ npm start      # fire up the project!
```

## Purpose

The purpose of the project is to provide and demonstrate the use of a simple authentication utility.

## Usage

For the most part, *server/services/authUtil.js* is self-contained. It could be imported into any application and used in a similar way as in this application assuming:

* There exists a *models/users.js* file in the *server* folder. This file must provide:

** A function **findByUserName** that returns a *mongo* document in a callback:
```javascript
/**
 *
 * @param {String} email - the email to find in the documents
 * @param {function({Object} err,{Object} doc)}
 *
 * calls the callback function:
 * if the email was found, err is null and doc contains the JSON doc
 * if the email was not found, err contains the error object, and doc is null
 */
```

** A function **createUser** that inserts a user into a document:
```javascript
/**
  *
  * @param {Object} user - user object to be inserted into database
  * @param {function({Object} err, {Object} result)}
  *
  * calls the callback function:
  * if the object was not inserted, err contains the error object err.code == 11000
  *   means that the email was a duplicate
  */
```

* To maximize modularity, a user is assumed to have 7 fields:
```javascript
/**
  * {
  *   "_id" :         {objectId},   
  *   "firstName" :   {String},
  *   "lastName" :    {String},
  *   "email" :       {String},
  *   "password" :    {String},     bcrypt hash
  *   "role" :        {String},     'admin', 'user', or anything else
  *   "data" :        {Object}      additional user data
  * }
  *
  */
```

* Implementation:
** Attach the *simpleAuth* middleware to the express application
** Call *createUserSession* wherever a user is initiating a session with the website for the first time (e.g., registration or login)
** Call *requireLogin* or *requirePermssions*, as a callback of the route where authorization is required

## Passwords for users:
lastName   | password   | role
------------------------------
faulkner   |.0Ld8n!m$+  | user
perez      |hm6U# eY`$  | user
hubbard    |3w\W}&5v$j  | user
solis      |0<bv0^YSC:  | admin
contreras  |ArwXfg'^2<  | user


  [rdegges svcc-auth]:https://github.com/rdegges/svcc-auth.git "rdegges svcc-auth github repository"
  [talk slides]:https://bitbucket.org/wbwatkinson/usafa_cs364_auth/raw/master/Everything_You_Ever_Wanted_to_Know_about_Authentication_in_Node.js.pdf?at=master&fileviewer=file-view-default "Everything You Ever Wanted to Know About Authentication in Node.js"
  [YouTube]:https://www.youtube.com/watch?v=yvviEA1pOXw "Everything You Ever Wanted to Know About Authentication in Node.js"
